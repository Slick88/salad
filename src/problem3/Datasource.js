const https = require('https');

class Datasource {
    static getMid = (commodity) => {return (commodity.buy + commodity.sell)/2}
    getPrices() {
        return new Promise((resolve, reject) => {
            https.get('https://static.ngnrs.io/test/prices', (resp) => {
                let data = '';

                resp.on('data', (chunk) => {
                    data += chunk;
                });

                resp.on('end', () => {
                    const parsedData = JSON.parse(data);
                    const processedData = parsedData.data.prices.map(commodity => {
                        console.log(commodity)
                        return {
                            pair: commodity.pair,
                            mid: () => {return (commodity.buy + commodity.sell)/2},
                            quote: () => {return commodity.pair.slice(3)}
                        }
                    })
                    resolve(processedData);
                });

            }).on("error", (err) => {
                console.log("Error: " + err.message);
                reject(err.message)
            });
        })
    }
}

const ds = new Datasource();
ds.getPrices()
    .then(prices => {
    prices.forEach(price => {
        console.log(price)
        console.log(`Mid price for ${ price.pair } is ${ price.mid() } ${ price.quote() }.`);
    });
}).catch(error => {
    console.error(error);
});
