const sum_to_n_a = function(n) {
    let sum = 0;
    for (let i = 1; i <= n; ++i) {
        sum += i;
    }
    return sum;
};

const sum_to_n_b = function(n) {
    if( n === 1) {
        return 1;
    } else {
        return n + sum_to_n_b(n-1)
    }
};

var sum_to_n_c = function(n) {
    const pairs = Math.floor(n/2);
    const pairValue = n + 1;
    const total = pairValue * pairs;
    if(n%2 !== 0) {
        return total + pairs + 1;
    } else return total;
};


const resulta = sum_to_n_a(6);
const resultb = sum_to_n_b(6);
const resultc = sum_to_n_c(6);

console.log('resulta: ', resulta)
console.log('resultb: ', resultb)
console.log('resultc: ', resultc)
